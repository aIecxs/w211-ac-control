#ifndef W211_CAN_B_H
#define W211_CAN_B_H

// ECU NAME: KLA_A1, ID: 0x0030, MSG COUNT: 22
typedef struct KLA_A1_t {
  bool LUEFTEN          : 1; // MSG NAME: LUEFTEN - Standheizung lüften, OFFSET 7, LENGTH 1
  bool HEIZEN           : 1; // MSG NAME: HEIZEN - Standheizung heizen, OFFSET 6, LENGTH 1
  bool LL_DZA           : 1; // MSG NAME: LL_DZA - Leerlauf-Drehzahlanhebung zur Kälteleistungserhöhung, OFFSET 5, LENGTH 1
  bool ZH_EIN_OK        : 1; // MSG NAME: ZH_EIN_OK - Zuheizer einschalten erlaubt, OFFSET 4, LENGTH 1
  bool ZWP_EIN          : 1; // MSG NAME: ZWP_EIN - Zusatzwasserpumpe einschalten, OFFSET 3, LENGTH 1
  bool IFG_EIN          : 1; // MSG NAME: IFG_EIN - Innenfühlergebläse einschalten, OFFSET 2, LENGTH 1
  bool EC_AKT           : 1; // MSG NAME: EC_AKT - EC-Modus aktiv, OFFSET 1, LENGTH 1
  bool HHS_EIN          : 1; // MSG NAME: HHS_EIN - Heizbare Heckscheibe einschalten, OFFSET 0, LENGTH 1

  uint8_t NLFTS         : 8; // MSG NAME: NLFTS - (%) Motorlüfter Solldrehzahl, OFFSET 8, LENGTH 8
  uint8_t M_KOMP        : 8; // MSG NAME: M_KOMP - (Nm) Drehmomentaufnahme Kältekompressor, OFFSET 16, LENGTH 8
  uint8_t KOMP_STELL    : 8; // MSG NAME: KOMP_STELL - (%) Kältekompressor Stellsignal, OFFSET 24, LENGTH 8

  uint8_t ZH_ANF        : 3; // MSG NAME: ZH_ANF - (Stufen) Anforderung Zuheizleistung, OFFSET 37, LENGTH 3
  bool ABVENT_W_ZU      : 1; // MSG NAME: ABVENT_W_ZU - Absperrventil Wärmetauscher schließen, OFFSET 36, LENGTH 1
  bool KJAL_ZU          : 1; // MSG NAME: KJAL_ZU - Kühlerjalousie schließen, OFFSET 35, LENGTH 1
  bool REST_AKT         : 1; // MSG NAME: REST_AKT - Restwärmebetrieb aktiv, OFFSET 34, LENGTH 1
  bool DEFROST_AKT      : 1; // MSG NAME: DEFROST_AKT - Defrostbetrieb aktiv, OFFSET 33, LENGTH 1
  bool                  : 1;

  uint8_t GEB_LSTG      : 8; // MSG NAME: GEB_LSTG - (%) Gebläse-Leistung, OFFSET 40, LENGTH 8

  uint8_t LKU_VORN      : 2; // MSG NAME: LKU_VORN - Stellung Lüftungsklappe unten, OFFSET 54, LENGTH 2
  //  MITTE             = 0, // enum: 0 - Mittelstellung / MITTE
  //  AUF               = 1, // enum: 1 - Stellung auf / AUF
  //  ZU                = 2, // enum: 2 - Stellung zu / ZU
  //  SNV               = 3, // enum: 3 - Signal invalid / SNV
  uint8_t LKM_VORN      : 2; // MSG NAME: LKM_VORN - Stellung Lüftungsklappe Mitte, OFFSET 52, LENGTH 2
  //  MITTE             = 0, // enum: 0 - Mittelstellung / MITTE
  //  AUF               = 1, // enum: 1 - Stellung auf / AUF
  //  ZU                = 2, // enum: 2 - Stellung zu / ZU
  //  SNV               = 3, // enum: 3 - Signal invalid / SNV
  uint8_t LKO_VORN      : 2; // MSG NAME: LKO_VORN - Stellung Lüftungsklappe oben, OFFSET 50, LENGTH 2
  //  MITTE             = 0, // enum: 0 - Mittelstellung / MITTE
  //  AUF               = 1, // enum: 1 - Stellung auf / AUF
  //  ZU                = 2, // enum: 2 - Stellung zu / ZU
  //  SNV               = 3, // enum: 3 - Signal invalid / SNV
  bool                  : 1;
  bool UL_AKT_KLA       : 1; // MSG NAME: UL_AKT_KLA - Umluft aktiv, OFFSET 48, LENGTH 1

  uint8_t T_INNEN_KLA   : 8; // MSG NAME: T_INNEN_KLA - (°C) Innentemperatur, OFFSET 56, LENGTH 8
} __attribute__((__packed__)) KLA_A1_t;

// ECU NAME: GW_C_B2, ID: 0x0003, MSG COUNT: 31
typedef struct GW_C_B2_t {
  uint8_t BLS_ST        : 2; // MSG NAME: BLS_ST - Status Bremslichtschalter, OFFSET 6, LENGTH 2
  //  BREMSE_NBET       = 0, // enum: 0 - Bremse nicht betätigt / BREMSE_NBET
  //  BREMSE_BET        = 1, // enum: 1 - Bremse betätigt / BREMSE_BET
  //  SNV               = 3, // enum: 3 - Signal nicht vorhanden / SNV
  bool BLS_UNT          : 1; // MSG NAME: BLS_UNT - Bremslichtunterdrückung, OFFSET 5, LENGTH 1
  bool BRE_AKT_ESP      : 1; // MSG NAME: BRE_AKT_ESP - ESP-Bremseneingriff aktiv, OFFSET 4, LENGTH 1
  bool LUEFT_MOT_KL     : 1; // MSG NAME: LUEFT_MOT_KL - Motorlüfter defekt Kontrolllampe, OFFSET 3, LENGTH 1
  bool KOMP_NOTAUS      : 1; // MSG NAME: KOMP_NOTAUS - Klima-Kompressor Not-Ausschalten, OFFSET 2, LENGTH 1
  bool KOMP_BAUS        : 1; // MSG NAME: KOMP_BAUS - Klima-Kompressor ausschalten: Beschleunigung, OFFSET 1, LENGTH 1
  bool ART_ABW_AKT      : 1; // MSG NAME: ART_ABW_AKT - ART-Abstandswarnung ist eingeschaltet, OFFSET 0, LENGTH 1

  uint8_t WHC           : 4; // MSG NAME: WHC - Getriebewählhebelstellung (nur NAG), OFFSET 12, LENGTH 4
  //  D                 = 5, // enum: 5 - Wählhebel in Stellung "D" / D
  //  N                 = 6, // enum: 6 - Wählhebel in Stellung "N" / N
  //  R                 = 7, // enum: 7 - Wählhebel in Stellung "R" / R
  //  P                 = 8, // enum: 8 - Wählhebel in Stellung "P" / P
  //  PLUS              = 9, // enum: 9 - Wählhebel in Stellung "+" / PLUS
  //  MINUS             =10, // enum: 10 - Wählhebel in Stellung "-" / MINUS
  //  N_ZW_D            =11, // enum: 11 - Wählhebel in Zwischenstellung "N-D" / N_ZW_D
  //  N_ZW_R            =12, // enum: 12 - Wählhebel in Zwischenstellung "N-R" / N_ZW_R
  //  P_ZW_R            =13, // enum: 13 - Wählhebel in Zwischenstellung "P-R" / P_ZW_R
  //  SNV               =15, // enum: 15 - Wählhebelposition unplausibel / SNV
  uint8_t BELADUNG      : 2; // MSG NAME: BELADUNG - Beladung, OFFSET 10, LENGTH 2
  //  LEER              = 0, // enum: 0 - Unbeladen / LEER
  //  HALB              = 1, // enum: 1 - Halb beladen / HALB
  //  VOLL              = 2, // enum: 2 - Voll beladen / VOLL
  //  SNV               = 3, // enum: 3 - Beladung nicht erkannt / SNV
  bool P                : 1; // MSG NAME: P - Parkstellung eingelegt, OFFSET 9, LENGTH 1
  bool RG               : 1; // MSG NAME: RG - Rückwärtsgang eingelegt (alle Getriebe), OFFSET 8, LENGTH 1

  uint8_t DRTGVL        : 2; // MSG NAME: DRTGVL - Drehrichtung Rad vorne links, OFFSET 16, LENGTH 2
  //  PASSIV            = 0, // enum: 0 - Keine Drehrichtungserkennung / PASSIV
  //  VOR               = 1, // enum: 1 - Drehrichtung vorwärts / VOR
  //  RUECK             = 2, // enum: 2 - Drehrichtung rückwärts / RUECK
  //  SNV               = 3, // enum: 3 - Signal nicht vorhanden / SNV
  uint16_t DVL          :14; // MSG NAME: DVL - (1/min) Raddrehzahl vorne links, OFFSET 18, LENGTH 14

  bool LTG_CHK_POS      : 1; // MSG NAME: LTG_CHK_POS - Leitungsüberwachung möglich, OFFSET 39, LENGTH 1
  bool KPL              : 1; // MSG NAME: KPL - Kupplung getreten, OFFSET 38, LENGTH 1
  bool INF_RFE_FSG      : 1; // MSG NAME: INF_RFE_FSG - FSG: EHB-ASG in Rückfallebene, OFFSET 37, LENGTH 1
  bool ST3_LEDL_DL      : 1; // MSG NAME: ST3_LEDL_DL - Linke LED 3-stufiger Schalter Dauerlicht, OFFSET 36, LENGTH 1
  bool ST3_LEDL_BL      : 1; // MSG NAME: ST3_LEDL_BL - Linke LED 3-stufiger Schalter Blinklicht, OFFSET 35, LENGTH 1
  bool ST3_LEDR_DL      : 1; // MSG NAME: ST3_LEDR_DL - Rechte LED 3-stufiger Schalter Dauerlicht, OFFSET 34, LENGTH 1
  bool ST3_LEDR_BL      : 1; // MSG NAME: ST3_LEDR_BL - Rechte LED 3-stufiger Schalter Blinklicht, OFFSET 33, LENGTH 1
  bool ST2_LED_DL       : 1; // MSG NAME: ST2_LED_DL - LED 2-stufiger Schalter Dauerlicht, OFFSET 32, LENGTH 1

  bool                  : 1;
  uint8_t WHST          : 3; // MSG NAME: WHST - Getriebewählhebelstellung (NAG, KSG, CVT), OFFSET 40, LENGTH 3
  //  P                 = 0, // enum: 0 - Getriebewählhebel in Stellung "P" / P
  //  R                 = 1, // enum: 1 - Getriebewählhebel in Stellung "R" / R
  //  N                 = 2, // enum: 2 - Getriebewählhebel in Stellung "N" / N
  //  D                 = 4, // enum: 4 - Getriebewählhebel in Stellung "D" / D
  //  SNV               = 7, // enum: 7 - Signal nicht vorhanden / SNV
  uint16_t MBRE_ESP     :12; // MSG NAME: MBRE_ESP - (Nm) Eingestelltes Bremsmoment, OFFSET 44, LENGTH 12

  bool LL_STBL          : 1; // MSG NAME: LL_STBL - Leerlauf ist stabil, OFFSET 63, LENGTH 1
  bool SUB_ABL_R        : 1; // MSG NAME: SUB_ABL_R - Substitution Abblendlicht rechts, OFFSET 62, LENGTH 1
  bool SUB_ABL_L        : 1; // MSG NAME: SUB_ABL_L - Substitution Abblendlicht links, OFFSET 61, LENGTH 1
  bool ZVB_EIN_MS       : 1; // MSG NAME: ZVB_EIN_MS - Zusatzverbraucher einschalten, OFFSET 60, LENGTH 1
  bool NOTBRE           : 1; // MSG NAME: NOTBRE - Notbremsung (Bremslicht blinken), OFFSET 59, LENGTH 1
  bool N_VRBT_SBCSH_AKT : 1; // MSG NAME: N_VRBT_SBCSH_AKT - SBC-S/H aktiv, ASG darf nicht nach "N" schalten, OFFSET 58, LENGTH 1
  bool ZH_AUS_MS        : 1; // MSG NAME: ZH_AUS_MS - Anforderung PTC-Zuheizer aus, OFFSET 57, LENGTH 1
  bool ZWP_EIN_MS       : 1; // MSG NAME: ZWP_EIN_MS - Zusatzwasserpumpe einschalten, OFFSET 56, LENGTH 1
} __attribute__((__packed__)) GW_C_B2_t;

// ECU NAME: SAM_V_A2, ID: 0x0017, MSG COUNT: 4
typedef struct SAM_V_A2_t {
  uint8_t T_AUSSEN_B    : 8; // MSG NAME: T_AUSSEN_B - (°C) Außenlufttemperatur, OFFSET 0, LENGTH 8
  uint16_t P_KAELTE     :16; // MSG NAME: P_KAELTE - (bar) Druck Kältemittel R134a, OFFSET 8, LENGTH 16
  uint16_t T_KAELTE     :16; // MSG NAME: T_KAELTE - (°C) Temperatur Kältemittel R134a, OFFSET 24, LENGTH 16
  uint8_t I_KOMP        : 8; // MSG NAME: I_KOMP - (mA) Strom Kompressor-Hauptregelventil, OFFSET 40, LENGTH 8
} __attribute__((__packed__)) SAM_V_A2_t;

#endif /* W211_CAN_B_H */
